package com.company;
import java.awt.event.*;

/**
 * Created by Adi on 03-Apr-17.
 */
public class Controller {

    private Model m;
    private View v;

    public Controller(Model mod, View vi){
        m = mod;
        v = vi;

        v.addStartListener(new StartListener());
        v.addStopListener(new StopListener());


    }

    class StartListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            m.startSimulation(v.getMinimumArrivalTime(), v.getMaximumArrivalTime(), v.getMinimumServiceTime(), v.getMaximumServiceTime(), v.getNumberOfQueues(), v, v.getSimulationTime());

        }
    }

    class StopListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.out.println("The simulation was stoped!");
            m.stopSimulation();
        }
    }

}
