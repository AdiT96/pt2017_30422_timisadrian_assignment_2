package com.company;

import static java.lang.System.exit;

/**
 * Created by Adi on 03-Apr-17.
 */
public class Model {

    private Store s;
    private Thread t;
    private boolean simulation = false;


    public Model(){

    }

    /*
    This method stops the simulation whena a button is pressed
     */
    public void stopSimulation(){
        s.terminateSimulation();
        simulation = false;
    }

    /*
    In this method, we check if the arguments given in the text fields are correct, if not, we display an appropriate
    message, otherwise, we start the simulation, only if the simulation has not been started yet
    Otherwise, the start simulation button could be pressed multiple times during a simulation
    and another simulation would run in parallel, making everything incomprehensible
     */
    public int startSimulation(String minA, String maxA, String minS, String maxS, String q, View v, String si){
        if (!simulation) {


            int miA, maA, miS, maS, qu, sim;
            try {
                miA = Integer.parseInt(minA);
                maA = Integer.parseInt(maxA);
                miS = Integer.parseInt(minS);
                maS = Integer.parseInt(maxS);
                qu = Integer.parseInt(q);
                sim = Integer.parseInt(si);
            } catch (NumberFormatException n) {
                System.out.println("Error at parsing Strings!");
                return 1;
            }

            if (qu <= 0) {
                System.out.println("Please input a positive number of queues!");
                return 1;
            }
            if (miA > maA) {
                System.out.println("Please input a valid minimum arrival time!");
                return 1;
            }
            if (miS > maS) {
                System.out.println("Please input a valid minimum service time!");
                return 1;
            }
            if (miA <= 0) {
                System.out.println("Please input a positive number for the minimum arrival time!");
                return 1;
            }
            if (maA <= 0) {
                System.out.println("Please input a positive number for the maximum arrival time!");
                return 1;
            }
            if (miS <= 0) {
                System.out.println("Please input a positive number for the minimum service time!");
                return 1;
            }
            if (maS <= 0) {
                System.out.println("Please input a positive number for the maximum service time!");
                return 1;
            }
            if (sim <= 0) {
                System.out.println("Please input a positive number for the simulation time!");
                return 1;
            }


            s = new Store(miA, maA, miS, maS, qu, v, sim);

            t = new Thread(s);

            simulation = true;
            t.start();



        }

        return 0;

    }


}
